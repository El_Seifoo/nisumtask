package com.nisum.itunes_task.retrofit

import com.nisum.itunes_task.network_model.AlbumApiResponse
import com.nisum.itunes_task.network_model.SearchApiResponse
import com.nisum.itunes_task.utils.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

fun ss(): OkHttpClient.Builder {

    var logging = HttpLoggingInterceptor()
    logging.level = HttpLoggingInterceptor.Level.BODY
    var httpClient = OkHttpClient.Builder()

//    if (BuildConfig.DEBUG)
    httpClient.addInterceptor(logging)

    return httpClient
}

private var retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create())
    .baseUrl(BASE_URL)
    .client(ss().build())
    .build()

object Network {
    val apiService: ApiService by lazy {
        retrofit.create(ApiService::class.java)
    }
}

interface ApiService {
    @GET(SEARCH)
    suspend fun search(
        @Query(TERM) query: String,
        @Query(MEDIA_KEY) media: String = MEDIA_VALUE,
        @Query(LIMIT_KEY) limit: Int = LIMIT_VALUE,
        @Query(OFFSET) offset: Int
    ): SearchApiResponse

    @GET(ALBUM_DETAILS)
    suspend fun fetchAlbum(
        @Query(ID) id: Long,
        @Query(ENTITY_KEY) entity: String = ENTITY_VALUE
    ): AlbumApiResponse
}