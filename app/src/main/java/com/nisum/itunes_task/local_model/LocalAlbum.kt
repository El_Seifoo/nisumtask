package com.nisum.itunes_task.local_model



class LocalAlbum(
    val albumId: Long,
    val albumName: String,
    val pic100: String?,
    val trackCount: Int,
    val copyright: String,
    val releaseDate: String,
    val genre: String?
)