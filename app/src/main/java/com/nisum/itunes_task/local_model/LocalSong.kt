package com.nisum.itunes_task.local_model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "song_table")
class LocalSong(
    @PrimaryKey
    @ColumnInfo(name = "track_id") val trackId: Long,
    @ColumnInfo(name = "album_id") val albumId: Long,
    @ColumnInfo(name = "album_name") val albumName: String,
    @ColumnInfo(name = "artist_name") val artistName: String?,
    @ColumnInfo(name = "track_name") val trackName: String,
    val genre: String?,
    @ColumnInfo(name = "track_photo") val pic100: String?,
    val length: Long?,
    @ColumnInfo(name = "searchQuery") val query: String
)