package com.nisum.itunes_task.utils

import android.annotation.SuppressLint
import android.content.res.Resources
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.nisum.itunes_task.R

@BindingAdapter("img_url")
fun ImageView.loadImage(url: String?) {
    Glide.with(this.context)
        .load(url ?: "")
        .apply(
            RequestOptions.placeholderOf(android.R.drawable.stat_sys_upload)
                .error(android.R.drawable.stat_notify_error)
        )
        .into(this)
}


/*
    converting milliSeconds time to readable time ( hours : minutes : seconds )
 */
@SuppressLint("SetTextI18n")
@BindingAdapter("milli_seconds")
fun TextView.setTime(time: Long) {
    val seconds = ((time / 1000) % 60).toInt()
    val secondsString = if (seconds < 10) "0$seconds" else seconds.toString()

    val minutes = ((time / (1000 * 60)) % 60).toInt()
    val minutesString = (if (minutes < 10) "0$minutes:" else "$minutes:")

    val hours = ((time / (1000 * 60 * 60)) % 24).toInt()
    val hoursString = if (hours == 0) "" else if (hours < 10) "0$hours:" else "$hours:"

    this.text = hoursString + minutesString + secondsString
}

@BindingAdapter("int_value")
fun TextView.setString(value: Int) {
    this.text = value.toString()
}

@SuppressLint("SetTextI18n")
@BindingAdapter("date")
fun TextView.formatDate(date: String?) {
    if (date != null) {
        val splitDate = date.split("T")[0].split("-")
        // Oct 6,2021
        this.text =
            "${
                stringMonth(
                    this.context.resources,
                    splitDate[1].toInt()
                )
            } ${splitDate[2]},${splitDate[0]}"
    } else
        this.text = ""

}

private fun stringMonth(res: Resources, monthNumb: Int): String {
    return when (monthNumb) {
        1 -> res.getString(R.string.january)
        2 -> res.getString(R.string.february)
        3 -> res.getString(R.string.march)
        4 -> res.getString(R.string.april)
        5 -> res.getString(R.string.may)
        6 -> res.getString(R.string.june)
        7 -> res.getString(R.string.july)
        8 -> res.getString(R.string.august)
        9 -> res.getString(R.string.september)
        10 -> res.getString(R.string.octorber)
        11 -> res.getString(R.string.november)
        else -> res.getString(R.string.december)

    }
}

