package com.nisum.itunes_task.utils


// URLS
public const val BASE_URL = "https://itunes.apple.com/"
public const val SEARCH = "search"
public const val ALBUM_DETAILS = "lookup"


// Requests Queries
public const val LIMIT_KEY = "limit"
public const val LIMIT_VALUE = 20
public const val MEDIA_KEY = "media"
public const val MEDIA_VALUE = "music"
public const val TERM = "term"
public const val OFFSET = "offset"
public const val ENTITY_KEY = "entity"
public const val ENTITY_VALUE = "song"
public const val ID = "id"

// Database name
public const val DATABASE_NAME = "nisumDB"