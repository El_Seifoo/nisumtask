package com.nisum.itunes_task.model


data class Song(
    val trackId: Long = 0,
    val albumId: Long = 0,
    val albumName: String = "",
    val artistName: String? = "",
    val trackName: String = "",
    val genre: String? = "",
    val pic100: String? = "",
    val length: Long? = 0
) {

}