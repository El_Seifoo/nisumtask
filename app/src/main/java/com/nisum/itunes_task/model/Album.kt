package com.nisum.itunes_task.model


class Album(
    val albumId: Long = 0,
    val albumName: String = "",
    val pic100: String? = "",
    val trackCount: Int = 0,
    val copyright: String = "",
    val releaseDate: String = "",
    val genre: String? = ""
)