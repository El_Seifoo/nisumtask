package com.nisum.itunes_task.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.nisum.itunes_task.local_model.LocalSong


@Dao
interface RoomDao {
    @Insert(onConflict = REPLACE)
    fun insertAll(localSongs: List<LocalSong>)

    @Query("SELECT * FROM song_table WHERE searchQuery =:query")
    fun getLocalSongs(query: String): LiveData<List<LocalSong>>

    @Query("SELECT DISTINCT searchQuery FROM song_table")
    fun getLocalQueries(): LiveData<List<String>>

    @Query("DELETE FROM song_table")
    fun deleteAllLocalQueries()
}