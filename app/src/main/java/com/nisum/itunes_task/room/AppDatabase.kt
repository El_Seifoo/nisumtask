package com.nisum.itunes_task.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.nisum.itunes_task.local_model.LocalSong
import com.nisum.itunes_task.utils.DATABASE_NAME

@Database(entities = [LocalSong::class], version = 2, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract val dbDao: RoomDao
}

private lateinit var INSTANCE: AppDatabase

fun getDBInstance(context: Context): AppDatabase {
    synchronized(AppDatabase::class.java) {
        if (!::INSTANCE.isInitialized) {
            INSTANCE = Room.databaseBuilder(
                context,
                AppDatabase::class.java,
                DATABASE_NAME
            )
                .fallbackToDestructiveMigration()
                .build()
        }
    }
    return INSTANCE
}