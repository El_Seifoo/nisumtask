package com.nisum.itunes_task.network_model

import com.nisum.itunes_task.model.Song
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class SearchApiResponse(
    @Json(name = "results") val songs: MutableList<NetworkSong>
)

fun SearchApiResponse.asModel(): MutableList<Song> {
    return songs.map {
        Song(
            trackId = it.trackId,
            albumId = it.albumId,
            albumName = it.albumName,
            artistName = it.artistName,
            trackName = it.trackName,
            genre = it.genre,
            pic100 = it.pic100,
            length = it.length,
        )
    }.toMutableList()
}