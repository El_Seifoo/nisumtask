package com.nisum.itunes_task.network_model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class NetworkSong(
    val trackId: Long,
    @Json(name = "collectionId") val albumId: Long,
    @Json(name = "collectionName") val albumName: String,
    val artistName: String?,
    val trackName: String,
    @Json(name = "primaryGenreName") val genre: String?,
    @Json(name = "artworkUrl100") val pic100: String?,
    @Json(name = "trackTimeMillis") val length: Long?
)

