package com.nisum.itunes_task.network_model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class NetworkAlbum(
    @Json(name = "collectionId") val albumId: Long,
    @Json(name = "collectionName") val albumName: String,
    @Json(name = "artworkUrl100") val pic100: String?,
    val trackCount: Int,
    val copyright: String,
    val releaseDate: String,
    @Json(name = "primaryGenreName") val genre: String?
)