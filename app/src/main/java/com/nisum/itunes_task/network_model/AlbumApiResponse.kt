package com.nisum.itunes_task.network_model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class AlbumApiResponse(
    @Json(name = "results")
    val data: MutableList<Any>
)


