package com.nisum.itunes_task.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nisum.itunes_task.adapter.OnItemClickListener
import com.nisum.itunes_task.adapter.SongsAdapter
import com.nisum.itunes_task.databinding.AlbumFragmentBinding
import com.nisum.itunes_task.view_model.album.AlbumViewModel
import com.nisum.itunes_task.view_model.album.AlbumViewModelFactory

class AlbumFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = AlbumFragmentBinding.inflate(inflater, container, false)

        val app = requireActivity().application
        val viewModelFactory =
            AlbumViewModelFactory(AlbumFragmentArgs.fromBundle(requireArguments()).albumId, app)
        val viewModel = ViewModelProvider(this, viewModelFactory).get(AlbumViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        viewModel.fetchedAlbum.observe(viewLifecycleOwner, Observer {
            it.let { binding.album = it }
        })

        val adapter = SongsAdapter(OnItemClickListener { return@OnItemClickListener })

        binding.songsList.layoutManager =
            LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        binding.songsList.adapter = adapter

        viewModel.fetchedSongs.observe(viewLifecycleOwner, Observer {
            it.let {
                adapter.submitList(it)
            }
        })

        return binding.root
    }
}