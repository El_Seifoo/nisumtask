package com.nisum.itunes_task.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nisum.itunes_task.adapter.OnItemClickListener
import com.nisum.itunes_task.adapter.OnQueryItemClickListener
import com.nisum.itunes_task.adapter.QueriesAdapter
import com.nisum.itunes_task.adapter.SongsAdapter
import com.nisum.itunes_task.databinding.SearchFragmentBinding
import com.nisum.itunes_task.mapper.toSongsList
import com.nisum.itunes_task.room.getDBInstance
import com.nisum.itunes_task.utils.LIMIT_VALUE
import com.nisum.itunes_task.view_model.search.SearchViewModel
import com.nisum.itunes_task.view_model.search.SearchViewModelFactory

class SearchFragment : Fragment() {
    private lateinit var viewModel: SearchViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = SearchFragmentBinding.inflate(inflater, container, false)

        binding.lifecycleOwner = this

        val app = requireActivity().application
        val database = getDBInstance(app)
        val viewModelFactory =
            SearchViewModelFactory(app, database)

        viewModel = ViewModelProvider(this, viewModelFactory).get(SearchViewModel::class.java)
        binding.viewModel = viewModel

        val adapter = SongsAdapter(OnItemClickListener {
            findNavController().navigate(
                SearchFragmentDirections.actionSearchFragmentToAlbumFragment(
                    it.albumId
                )
            )
        })


        binding.songsList.layoutManager =
            LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        binding.songsList.adapter = adapter

        addListScrollListener(binding.songsList, viewModel)

        viewModel.fetchedSongs.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                Log.e("count", it.size.toString())
            } else {
                Log.e("count", "null")
            }
            adapter.submitList(it)
            viewModel.isSearchStarting(searching = false)
        })

        var localQuery: String = ""
        val searchHistoryAdapter = QueriesAdapter(OnQueryItemClickListener {
            localQuery = it
            viewModel.isSearchStartingLocally(true)
        })

        viewModel.startSearchingLocally.observe(viewLifecycleOwner, Observer {
            if (it) {
                viewModel.searchLocally(localQuery)
                viewModel.isSearchStartingLocally(false)
            }
        })

        binding.searchHistoryList.layoutManager =
            LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        binding.searchHistoryList.adapter = searchHistoryAdapter

        viewModel.localQueries.observe(viewLifecycleOwner, Observer {
            searchHistoryAdapter.submitList(it)
            viewModel.isSearchStartingLocally(searching = false)
        })


        viewModel.localData.observe(viewLifecycleOwner, Observer {
            if (it.isNullOrEmpty())
                adapter.submitList(it.toSongsList())
            else {
                adapter.submitList(null)
                viewModel.isSearchStartingLocally(false)
            }
        })

        viewModel.startSearching.observe(viewLifecycleOwner, Observer
        {
            if (it) {
                viewModel.requestQuery()
                viewModel.isSearchStarting(searching = false)
            }
        })

        return binding.root
    }


    /*
        handling pagination
     */
    var isLoading = false
    var isLastPage = false
    var isScrolling = false

    private fun addListScrollListener(recyclerView: RecyclerView, viewModel: SearchViewModel) {
        val scrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                if (dy > 0) {
                    val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                    val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
                    val visibleItemCount = layoutManager.childCount
                    val totalItemCount = layoutManager.itemCount

                    val isNotLoadingAndNotLastPage = !isLoading && !isLastPage
                    val isAtLastItem = firstVisibleItemPosition + visibleItemCount >= totalItemCount
                    val isNotAtBeginning = firstVisibleItemPosition >= 0
                    val isTotalMoreThanVisible = totalItemCount >= LIMIT_VALUE
                    val shouldPaginate =
                        isNotLoadingAndNotLastPage && isAtLastItem && isNotAtBeginning &&
                                isTotalMoreThanVisible && isScrolling
                    if (shouldPaginate) {
                        viewModel.requestQuery()
                        isScrolling = false
                    }
                }

            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                    isScrolling = true
            }
        }
        recyclerView.addOnScrollListener(scrollListener)

    }
}
