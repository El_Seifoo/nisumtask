package com.nisum.itunes_task.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nisum.itunes_task.databinding.SearchHistoryItemBinding

class QueriesAdapter(private val clickListener: OnQueryItemClickListener) :
    ListAdapter<String, QueriesAdapter.ViewHolder>(QueriesDiffUtil()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }


    class ViewHolder private constructor(private val binding: SearchHistoryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(query: String, clickListener: OnQueryItemClickListener) {
            binding.clickListener = clickListener
            binding.query = query
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = SearchHistoryItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

class OnQueryItemClickListener(private val clickListener: (string: String) -> Unit) {
    fun onQueryClick(string: String) = clickListener(string)
}

class QueriesDiffUtil : DiffUtil.ItemCallback<String>() {

    override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
        return oldItem == newItem
    }
}