package com.nisum.itunes_task.adapter


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nisum.itunes_task.databinding.SearchItemBinding
import com.nisum.itunes_task.model.Song

class SongsAdapter(private val clickListener: OnItemClickListener) :
    ListAdapter<Song, SongsAdapter.ViewHolder>(SongsDiffUtil()) {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }

    class ViewHolder private constructor(private val binding: SearchItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(song: Song, clickListener: OnItemClickListener) {
            binding.clickListener = clickListener
            binding.song = song
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = SearchItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

}


class OnItemClickListener(private val clickListener: (song: Song) -> Unit) {
    fun onSongClick(song: Song) = clickListener(song)
}

class SongsDiffUtil : DiffUtil.ItemCallback<Song>() {
    override fun areItemsTheSame(oldItem: Song, newItem: Song): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Song, newItem: Song): Boolean {
        return oldItem.trackId == newItem.trackId
    }
}