package com.nisum.itunes_task.view_model.album

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class AlbumViewModelFactory(
    private val albumId: Long,
    private val app: Application,
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AlbumViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return AlbumViewModel(albumId,app) as T
        }
        throw IllegalArgumentException("Unable to construct viewmodel")
    }
}