package com.nisum.itunes_task.view_model.search

import android.app.Application
import android.util.Log
import android.view.View
import androidx.lifecycle.*
import com.nisum.itunes_task.local_model.LocalSong
import com.nisum.itunes_task.mapper.asLocalSongsList
import com.nisum.itunes_task.model.Song
import com.nisum.itunes_task.retrofit.Network
import com.nisum.itunes_task.network_model.SearchApiResponse
import com.nisum.itunes_task.network_model.asModel
import com.nisum.itunes_task.room.AppDatabase
import com.nisum.itunes_task.utils.LIMIT_VALUE
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SearchViewModel(private val app: Application, private val database: AppDatabase) :
    AndroidViewModel(app) {

    private val _fetchedSongs = MutableLiveData<List<Song>?>()
    val fetchedSongs: LiveData<List<Song>?>
        get() = _fetchedSongs


    private val _searchHistoryContainerVisibility = MutableLiveData<Int>(View.GONE)
    val searchHistoryContainerVisibility: LiveData<Int>
        get() = _searchHistoryContainerVisibility

    private val _clearSubmitIconsVisibility = MutableLiveData<Int>(View.GONE)
    val clearSubmitIconsVisibility: LiveData<Int>
        get() = _clearSubmitIconsVisibility


    private val _startSearching = MutableLiveData<Boolean>()
    val startSearching: LiveData<Boolean>
        get() = _startSearching


    fun isSearchStarting(searching: Boolean) {
        _startSearching.value = searching
    }


    private val _startSearchingLocally = MutableLiveData<Boolean>()
    val startSearchingLocally: LiveData<Boolean>
        get() = _startSearchingLocally

    fun isSearchStartingLocally(searching: Boolean) {
        _startSearchingLocally.value = searching
    }

    var query = MutableLiveData<String>("")
    var prevQuery = ""
    private var offset = 0
    private var songsResponse: SearchApiResponse? = null

    init {
        isSearchStarting(searching = false)
        isSearchStartingLocally(searching = false)
        _searchHistoryContainerVisibility.value = View.VISIBLE
        _clearSubmitIconsVisibility.value = View.GONE
    }

    fun requestQuery() {
        viewModelScope.launch {
            try {
                var query = query.value
                // check is user is changing the query or changing the page
                // if prev query and query are not the same so we have to clear list and offset
                if (prevQuery != query) {
                    offset = 0
                    songsResponse = null
                }

                val searchApiResponse =
                    Network.apiService.search(query = query!!.replace(" ", "+"), offset = offset)

                val songs = searchApiResponse.asModel()
                if (songs.isNotEmpty())
                    offset += LIMIT_VALUE

                if (songsResponse == null) {
                    songsResponse = searchApiResponse
                } else {
                    val oldSongs = songsResponse?.songs
                    oldSongs?.addAll(searchApiResponse.songs)
                }



                _fetchedSongs.value = songsResponse?.asModel()
                prevQuery = query

            } catch (e: Exception) {
                Log.e("exception", e.message.toString())
            }
        }
    }

    val localQueries: LiveData<List<String>> = database.dbDao.getLocalQueries()


    /*
        listen to the change of the text of the edit text
     */
    fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
        if (text != null)
            if (text.isNotEmpty()) {

                _searchHistoryContainerVisibility.value = View.GONE

                // show clear text icon when user write any input
                // show submit icon when user write any input
                _clearSubmitIconsVisibility.value = View.VISIBLE

                // tell view model to start searching when user write any input
                query.value = text.toString()
                isSearchStarting(true)


                // view model to start searching
            } else {
                // when user clear his search text
                _fetchedSongs.value = null

                // hide clear icon because there is nothing to clear
                // hide submit btn because there is nothing to submit
                _clearSubmitIconsVisibility.value = View.GONE
                // show search history list
                _searchHistoryContainerVisibility.value = View.VISIBLE
            }
    }

    val localData = MutableLiveData<List<LocalSong>>()
    fun searchLocally(query: String) {
        Log.e("query", query)
        localData.value = database.dbDao.getLocalSongs(query).value
    }

    /*
        functions for pressed btns
     */
    fun onSubmitBtnClicked() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                database.dbDao.insertAll(songsResponse!!.songs.asLocalSongsList(query.value!!))
            }
        }
    }


    fun onClearBtnClicked() {
        query.value = ""
        prevQuery = ""
        _fetchedSongs.value = null
    }

    fun clearSearchHistory() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                database.dbDao.deleteAllLocalQueries()
            }
        }
    }


}