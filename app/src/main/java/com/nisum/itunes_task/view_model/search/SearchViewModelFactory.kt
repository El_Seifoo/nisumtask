package com.nisum.itunes_task.view_model.search

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nisum.itunes_task.room.AppDatabase

class SearchViewModelFactory(val app: Application, private val database: AppDatabase) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SearchViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return SearchViewModel(app, database) as T
        }
        throw IllegalArgumentException("Unable to construct viewmodel")
    }
}