package com.nisum.itunes_task.view_model.album

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.nisum.itunes_task.mapper.asAlbum
import com.nisum.itunes_task.mapper.asSongsList
import com.nisum.itunes_task.model.Album
import com.nisum.itunes_task.model.Song
import com.nisum.itunes_task.network_model.NetworkAlbum
import com.nisum.itunes_task.network_model.NetworkSong
import com.nisum.itunes_task.retrofit.Network
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import kotlinx.coroutines.launch
import java.lang.Exception

class AlbumViewModel(
    private val albumId: Long,
    private val app: Application,
) :
    AndroidViewModel(app) {
    private val _fetchedAlbum = MutableLiveData<Album>()
    val fetchedAlbum: LiveData<Album>
        get() = _fetchedAlbum

    private val _fetchedSongs = MutableLiveData<List<Song>>()
    val fetchedSongs: LiveData<List<Song>>
        get() = _fetchedSongs

    init {
        requestAlbum()
    }

    private fun requestAlbum() {
        viewModelScope.launch {
            try {
                val albumApiResponse = Network.apiService.fetchAlbum(albumId)
                val albumSongsList = albumApiResponse.data

                val moshi = Moshi.Builder().build()

                val typeNetworkSong =
                    Types.newParameterizedType(List::class.java, NetworkSong::class.java)

                val adapterNetworkAlbum = moshi.adapter(NetworkAlbum::class.java)
                val adapterNetworkSong = moshi.adapter<List<NetworkSong>>(typeNetworkSong)

                val networkAlbum = adapterNetworkAlbum.fromJsonValue(albumApiResponse.data[0])
                albumSongsList.removeAt(0)
                val networkSong = adapterNetworkSong.fromJsonValue(albumApiResponse.data)

                _fetchedAlbum.value = networkAlbum?.asAlbum()
                _fetchedSongs.value = networkSong?.asSongsList()


            } catch (exception: Exception) {
                Log.e("ee", exception.message.toString())
            }
        }
    }
}

