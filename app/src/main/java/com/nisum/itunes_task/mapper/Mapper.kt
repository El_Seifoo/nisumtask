package com.nisum.itunes_task.mapper

import com.nisum.itunes_task.local_model.LocalSong
import com.nisum.itunes_task.model.Album
import com.nisum.itunes_task.model.Song
import com.nisum.itunes_task.network_model.NetworkAlbum
import com.nisum.itunes_task.network_model.NetworkSong

fun NetworkAlbum.asAlbum(): Album {
    return Album(
        albumId = this.albumId,
        albumName = this.albumName,
        pic100 = this.pic100,
        trackCount = this.trackCount,
        copyright = this.copyright,
        releaseDate = this.releaseDate,
        genre = this.genre
    )
}
fun List<LocalSong>.toSongsList():MutableList<Song>{
    return this.map {
        Song(
            trackId = it.trackId,
            albumId = it.albumId,
            albumName = it.albumName,
            artistName = it.artistName,
            trackName = it.trackName,
            genre = it.genre,
            pic100 = it.pic100,
            length = it.length,
        )
    }.toMutableList()
}

fun List<NetworkSong>.asSongsList(): MutableList<Song> {
    return this.map {
        Song(
            trackId = it.trackId,
            albumId = it.albumId,
            albumName = it.albumName,
            artistName = it.artistName,
            trackName = it.trackName,
            genre = it.genre,
            pic100 = it.pic100,
            length = it.length,
        )
    }.toMutableList()
}

fun List<NetworkSong>.asLocalSongsList(query: String): List<LocalSong> {
    return this.map {
        LocalSong(
            trackId = it.trackId,
            albumId = it.albumId,
            albumName = it.albumName,
            artistName = it.artistName,
            trackName = it.trackName,
            genre = it.genre,
            pic100 = it.pic100,
            length = it.length,
            query = query
        )
    }
}

fun NetworkSong.asSong(): Song {
    return Song(
        trackId = this.trackId,
        albumId = this.albumId,
        albumName = this.albumName,
        artistName = this.artistName,
        trackName = this.trackName,
        genre = this.genre,
        pic100 = this.pic100,
        length = this.length,
    )
}


